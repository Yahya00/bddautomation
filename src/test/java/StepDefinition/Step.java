package StepDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.junit.Assert.assertTrue;

public class Step {

    WebDriver driver;
    WebDriverWait wait;

    @Given("^Open the chrome and launch the application$")
    public void Open_the_chrome_and_launch_the_application() throws Throwable {
    //    System.out.println("This step open the chrome browser and launch the applocation");
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demo.guru99.com/v4");
        wait = new WebDriverWait(driver,10);
    }

    @When("^Enter the username and password$")
    public void Enter_the_username_and_password() throws Throwable {
    //    System.out.println("This step enter the username and password on the login page");
        driver.findElement(By.name("uid")).sendKeys("mngr417983");
        driver.findElement(By.name("password")).sendKeys("ypanUpY");
        driver.findElement(By.name("btnLogin")).click();
    }

    @Then("^Login to website$")
    public void Login_to_website() throws Throwable {
       // System.out.println("This step click on the login");

        WebElement title = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("heading3")));
        assertTrue(title.isDisplayed());

        Thread.sleep(3000);
        driver.quit();
    }

}
